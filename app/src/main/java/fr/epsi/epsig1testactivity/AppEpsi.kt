package fr.epsi.epsig1testactivity

import android.app.Application
import android.widget.Toast

class AppEpsi : Application(){

    override fun onCreate() {
        super.onCreate()
    }

    fun showToast(txt : String){
        Toast.makeText(this,txt,Toast.LENGTH_SHORT).show()
    }
}