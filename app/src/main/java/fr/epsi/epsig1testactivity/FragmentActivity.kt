package fr.epsi.epsig1testactivity

import android.os.Bundle
import android.view.View
import android.widget.TextView

class FragmentActivity : BaseActivity() {
    val tab1Fragment=Tab1Fragment.newInstance("DJEMAM","NIDAL")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragmet)
        val tab1=findViewById<TextView>(R.id.textViewTab1)
        val tab2=findViewById<TextView>(R.id.textViewTab2)
        showBack()

        tab1.setOnClickListener(View.OnClickListener {
            showTab1()
        })

        tab2.setOnClickListener(View.OnClickListener {
            showTab2()
        })
        showTab1()
    }

    private fun showTab2() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.addToBackStack("Tab2Fragment") // name can be null
        fragmentTransaction.replace(R.id.fragment_container, MapsFragment::class.java, null)
        fragmentTransaction.commit()
    }

    private fun showTab1() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.addToBackStack("Tab1Fragment") // name can be null
        fragmentTransaction.replace(R.id.fragment_container, tab1Fragment, null)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount>1)
            super.onBackPressed()
        else
            finish()
    }
}