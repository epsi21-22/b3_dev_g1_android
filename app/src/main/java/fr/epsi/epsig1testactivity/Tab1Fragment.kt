package fr.epsi.epsig1testactivity

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Tab1Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Tab1Fragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2) + Calendar.getInstance().time.toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val editTextEmail= view.findViewById<EditText>(R.id.editTextEmail)
        val editTextFirstName= view.findViewById<EditText>(R.id.editTextFirstName)
        val editTextLastName= view.findViewById<EditText>(R.id.editTextLastName)
        val buttonSave = view.findViewById<Button>(R.id.buttonSave)
        buttonSave.setOnClickListener(View.OnClickListener {
            writeSharedPref("firstName",editTextFirstName.text.toString())
            writeSharedPref("lastName",editTextLastName.text.toString())
            writeSharedPref("email",editTextEmail.text.toString())
        })

        editTextEmail.setText(readSharedPref("email"))
        editTextFirstName.setText(readSharedPref("firstName"))
        editTextLastName.setText(readSharedPref("lastName"))
    }

    fun writeSharedPref(key:String, value:String){
        activity?.let {
            val sharedPreferences: SharedPreferences =
                it.getSharedPreferences("account", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(key, value)
            editor.apply()
        }
    }

    fun readSharedPref(key:String):String{
        activity?.let{
            val sharedPreferences: SharedPreferences = it.getSharedPreferences("account", Context.MODE_PRIVATE)
            return sharedPreferences.getString(key,"not found").toString()
        }
        return ""
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Tab1Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Tab1Fragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}