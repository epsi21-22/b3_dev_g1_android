package fr.epsi.epsig1testactivity

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class SaveDataActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_data)
        val editTextEmail= findViewById<EditText>(R.id.editTextEmail)
        val editTextFirstName= findViewById<EditText>(R.id.editTextFirstName)
        val editTextLastName= findViewById<EditText>(R.id.editTextLastName)
        val buttonSave = findViewById<Button>(R.id.buttonSave)
        showBack()
        buttonSave.setOnClickListener(View.OnClickListener {
            writeSharedPref("firstName",editTextFirstName.text.toString())
            writeSharedPref("lastName",editTextLastName.text.toString())
            writeSharedPref("email",editTextEmail.text.toString())
        })

        editTextEmail.setText(readSharedPref("email"))
        editTextFirstName.setText(readSharedPref("firstName"))
        editTextLastName.setText(readSharedPref("lastName"))
    }


    fun writeSharedPref(key:String,value:String){
        val sharedPreferences: SharedPreferences= getSharedPreferences("account",Context.MODE_PRIVATE)
        val editor =sharedPreferences.edit()
        editor.putString(key,value)
        editor.apply()
    }

    fun readSharedPref(key:String):String{
        val sharedPreferences: SharedPreferences= getSharedPreferences("account",Context.MODE_PRIVATE)
        return sharedPreferences.getString(key,"not found").toString()
    }
}