package fr.epsi.epsig1testactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setHeaderTitle("Login")
        showBack()
        val editTextEmail = findViewById<EditText>(R.id.editTextEmail)
        val editTextPassword = findViewById<EditText>(R.id.editTextPassword)
        val buttonGo = findViewById<Button>(R.id.buttonGo)
        buttonGo.setOnClickListener(View.OnClickListener {
            (application as AppEpsi).showToast(editTextEmail.text.toString()+"/"+editTextPassword.text.toString())
        })
    }
}