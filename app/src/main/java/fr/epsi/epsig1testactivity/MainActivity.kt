package fr.epsi.epsig1testactivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button


class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttonNature = findViewById<Button>(R.id.button_home_nature)
        val buttonEspace = findViewById<Button>(R.id.button_home_espace)
        val buttonLogin = findViewById<Button>(R.id.button_home_login)
        val buttonStudent = findViewById<Button>(R.id.button_home_student)
        val buttonStudentOnline = findViewById<Button>(R.id.button_home_student_online)
        val buttonSharedPref = findViewById<Button>(R.id.button_home_shared_pref)
        val buttonFragment = findViewById<Button>(R.id.button_home_fragment)


        buttonNature.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,DetailActivity::class.java)
            newIntent.putExtra("title","Nature")
            newIntent.putExtra("urlImage","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROFiHOkKUHVBF3TcyU1NgawBlIV9mIoSGAuA&usqp=CAU")
            startActivity(newIntent)
        })

        buttonEspace.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,DetailActivity::class.java)
            newIntent.putExtra("title","Espace")
            newIntent.putExtra("urlImage","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRj0AEwRdUSWfs2LPDlLKn9kI-KvverDKfy0w&usqp=CAU")
            startActivity(newIntent)
        })

        buttonLogin.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,LoginActivity::class.java)
            startActivity(newIntent)
        })

        buttonStudent.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,StudentActivity::class.java)
            startActivity(newIntent)
        })

        buttonStudentOnline.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,StudentOnlineActivity::class.java)
            startActivity(newIntent)
        })

        buttonSharedPref.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,SaveDataActivity::class.java)
            startActivity(newIntent)
        })

        buttonFragment.setOnClickListener(View.OnClickListener {
            val newIntent = Intent(application,FragmentActivity::class.java)
            startActivity(newIntent)
        })

        setHeaderTitle("Main")
        (application as AppEpsi).showToast("Hello Epsi")
    }
}